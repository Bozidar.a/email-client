import Vue from 'vue'

import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({

	state: {

		status: 'inbox',

		messages: [],

		message: null
	},

	getters: {

		status: state 	=> state.status,

		messages: state => state.messages,

		message: state 	=> state.message,
	},

	mutations: {

		updateStatus(state, value) {

			Vue.set(state, 'status', value)
		},

		getMessages(state, value) {

			Vue.set(state, 'messages', value)
		},

		getMessage(state, value) {

			Vue.set(state, 'message', value)
		}
	}
})