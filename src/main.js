import Vue from 'vue'

import App from './App.vue'

Vue.config.productionTip = false

const files = require.context('./', true, /\.vue$/i); files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

import { store } from './store'

import './assets/styles/app.scss'

import './filters.js'

new Vue({
	
	render: h => h(App),

	store

}).$mount('#app')
