import Vue from 'vue' 

// Create timestamp from string
Vue.filter('timestamp', function (value) {
    
    if (!value) return ''
    
    let newDate = new Date(parseInt(value))

    let options = { 

        // Make 24 hour display
        hour12: false
    }

    return newDate.toLocaleString('de', options)
})

// Substring text for preview
Vue.filter('substring', function (value, data) {
    
    if (!value) return ''

    return value.substring(0, data).concat('...')
})